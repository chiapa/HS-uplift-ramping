import pandas as pd
import numpy as np
import os
from datetime import datetime
from contextlib import closing
import cx_Oracle
from xlsxwriter.utility import xl_cell_to_rowcol
from openpyxl import Workbook, load_workbook
import subprocess
from aurora_token import get_token


def get_dwh_password():
    return get_token(
        user_name="production",
        service="DWH",
        enable_keyring=True,
        enable_ssm=False,
        environment_variable="PGPASSWORD",
        key_file=".aurora-dwh-key",
        interactive=False,
    )


def get_oracle_password():
    return get_token(
        user_name="ANALYSTACCESS",
        service="Oracle",
        enable_keyring=True,
        enable_ssm=False,
        environment_variable="Not_a_variable",
        key_file=".aurora-oracle-key",
        interactive=False,
    )


def listify(x):
    if x is None:
        return None
    return x if type(x) is list else [x]


def deterministic_freq(n, p) -> np.ndarray:
    """
    Return a numpy.ndarray of length n in which close to p proportion of entries are 1 and the rest are 0, in a way that
    will give the same result for the same input every time (deterministic). default startung value is 0 (unless p >= 1)
    .
    :param n: length
    :param p: probablility of 1
    :return:
    """
    if p >= 1:
        return np.full(n, 1)
    exist = np.full(n, 0)
    n_occur = exist[0]
    for i in range(1, n):
        if n_occur / i < p:
            exist[i] = 1
            n_occur += 1
    return exist


def EOS_time(df: pd.DataFrame, keep=False, on="", **kwargs) -> pd.DataFrame:
    def dt_HH(x):
        return datetime(x // 100000000, (x % 100000000) // 1000000, (x % 1000000) // 10000, (x % 10000) // 100, x % 100)

    def dt_DD(x):
        return datetime(x // 10000, (x % 10000) // 100, x % 100, 0, 0)

    def dt_MM(x):
        return datetime(x // 100, x % 100, 1, 0, 0)

    funcs = [("HH_ID", dt_HH), ("DD_ID", dt_DD), ("MM_ID", dt_MM)]
    for ttype, func in funcs:
        if ttype in df.columns:
            if "DateTime" not in df.columns:
                df.insert(0, "DateTime", df[ttype].apply(func))
                if not keep:
                    df.drop(ttype, axis=1, inplace=True)
            elif "DateTime_" + ttype not in df.columns:
                df.insert(0, "DateTime_" + ttype, df[ttype].apply(func))
                if not keep:
                    df.drop(ttype, axis=1, inplace=True)
        if df.index.name == ttype:
            if keep:
                newcol = ttype
                while newcol in df.columns:
                    newcol += "_"
                df[newcol] = df.index
            df.index = df.index.map(func)
            df.index.name = "DateTime"
    return df


def connect_string(dictionary):
    """Prepare the database connection string from the config dictionary"""
    connect_string = (
        dictionary["username"]
        + "/"
        + dictionary["password"]
        + "@"
        + dictionary["hostname"]
        + ":"
        + dictionary["port"]
        + "/"
        + dictionary["service"]
    )
    return connect_string


def get_outname(basename, ext, outfol="Outputs", make_timefol=False):
    if ext[0] != ".":
        ext = "." + ext
    if make_timefol:
        outfol = get_nowfolder(basefol=outfol)
    if not os.path.exists(outfol):
        os.makedirs(outfol)
    outfile = os.path.join(outfol, basename + ext)
    i = 0
    while os.path.isfile(outfile):
        outfile = os.path.join(outfol, basename + "_" + str(i) + ext)
        i += 1
    return outfile


def get_nowfolder(basefol="Outputs"):
    nowfol = os.path.join(basefol, datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
    if not os.path.exists(nowfol):
        os.makedirs(nowfol)
    return nowfol


def dict_to_list(d):
    outl = []
    keys = list(d.keys())
    for key in keys:
        if type(d[key]) is dict:
            outl.append([key + ": "])
            sub = dict_to_list(d[key])
            for s in sub:
                outl.append(["    "] + s)
        else:
            outl.append([key + ": ", d[key]])
    return outl


def dict_to_str(d, l_buffer=0):
    out = ""
    keys = list(d)
    for key in keys:
        out += " " * l_buffer + key + ": "
        if type(d[key]) is dict:
            out += "\n" + dict_to_str(d[key], l_buffer=l_buffer + 4)
        else:
            out += str(d[key]) + "\n"
    return out


def populate_excel_search(
    data,
    file: str = None,
    sheets=None,
    columns=None,
    save_file: str = None,
    lose_graphs=True,
    wbook: Workbook = None,
    do_save=True,
    **kwargs,
) -> Workbook:
    """
    put data from a pandas dataframe into excel from file or openpyxl workbook by finding text in the excel which is the
    same as a column in the dataframe (label). the data from the column will be trasposed, and can be more then one
    column for each label if a dataframe with multiindex columns is passed
    :param data: dataframe with labels in 0th column index, list of such
    :param file: name of excel file to be read if desired
    :param sheets: sheets which need data polulation
    :param columns: columns containing the labels to indicate positions for population
    :param save_file: name of resulting excel file if so_save=True
    :param lose_graphs: not yetimplemented
    :param wbook: openpyxl Workbook if populating from workbook instead of excel file
    :param do_save: save exel file to disk using name provided by save_file
    :param kwargs: arguments to be passed to populate excel
    :return: populated excel
    """
    data = listify(data)
    sheets = listify(sheets)
    columns = listify(columns)
    if wbook is None:
        wbook = load_workbook(file, read_only=False)
    if sheets is None:
        sheets = [s.title for s in wbook.worksheets]
    for sheet in sheets:
        wsheet = wbook[sheet]
        if columns is None:
            columns = list(range(1, wsheet.max_column + 1))
        max_row = wsheet.max_row
        for col in columns:
            if type(col) is str:
                col = xl_cell_to_rowcol(col + "1")[1] + 1
            for row in range(1, max_row + 1):
                val = wsheet.cell(row=row, column=col).value
                for datum in data:
                    if val in datum.columns.get_level_values(0).unique():
                        wbook = populate_excel(
                            data=datum[[val]].values.transpose(),
                            sheet=sheet,
                            wbook=wbook,
                            do_save=False,
                            position=(row, col),
                            lose_graphs=lose_graphs,
                            **kwargs,
                        )
                        break
    if do_save:
        wbook.save(save_file if save_file else file)
    return wbook


def populate_excel(
    data: np.ndarray,
    file: str = None,
    sheet: str = None,
    position="A1",
    save_file: str = None,
    lose_graphs=True,
    wbook: Workbook = None,
    do_save=True,
    position_offset=None,
) -> Workbook:
    """
    Puts data from a numpy ndarray into excel.
    :param data: data to go into excel
    :param file: name of file to load, can leave out if specifying wbook. wbook will take priority over this
    :param sheet: name of sheet in excel file to have data inserted
    :param position: top left corner of rectangle for data to go either string e.g. 'A1' or indexable (start row, start column)
    :param save_file: name of file to save result. If not specified original file is over-written
    :param lose_graphs: keeping graphs requires entirely different operation. Not yet implemented
    :param wbook: can specify existing openpyxl Workbook instead of leading an excel file
    :param do_save: whether or not to save the result to disk
    :return: the Workbook which was populated
    """
    if not lose_graphs:
        print("keeping graphs not yet implemented")
        return
    if type(position) is str:
        (srow, scol) = xl_cell_to_rowcol(position)
        srow += 1
        scol += 1
    else:
        (srow, scol) = tuple(position)
    if wbook is None:
        wbook = load_workbook(file, read_only=False)
    if sheet is not None:
        wsheet = wbook[sheet]
    else:
        wsheet = wbook.active
    (r_off, c_off) = position_offset if position_offset is not None else (0, 0)
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            wsheet.cell(row=srow + i + r_off, column=scol + j + c_off).value = data[i][j]
    if do_save:
        wbook.save(save_file if save_file else file)
    return wbook


################# GETTING DATA FROM DATABASE #############
def query(sql, **kwargs) -> pd.DataFrame:
    dbcfg = {
        "hostname": "oracle.auroraer.com",
        "port": "1521",
        "service": "ORCL",
        "username:": "ANALYSTACCESS",
        "password": get_oracle_password(),
    }
    with closing(cx_Oracle.connect(connect_string(dbcfg))) as conn:
        df = pd.read_sql_query(sql, conn)
    df = EOS_time(df, **kwargs)
    return df


def query_years(sql, years=None, **kwargs) -> pd.DataFrame:
    if years is None:
        years = list(range(2010, datetime.now().year + 1))
    dfs = []
    for year in years:
        dfs.append(query(sql.format(year), **kwargs))
    df = pd.concat(dfs)
    return df


def prepare_data_db(dataset, start_year=None, end_year=None, resm=None, **kwargs):
    """
    retrieve and clean data from the database
    :param dataset:
    :param start_year: inclusive
    :param end_year: inclusive
    :param resm:
    :param kwargs:
    :return:
    """

    def final_form(df, resm=False, **kwargs):
        df.set_index("DateTime", inplace=True)
        if resm:
            df = df.resample("m").mean()
        df.insert(0, "year", df.index.year)
        df.insert(1, "month", df.index.month)
        return df

    if dataset == "fuel":
        sql = "SELECT DD_ID, UKGAS, UKCOAL, UKOIL, UKCO2 FROM EOS_GLOBAL_COMMODITY.COMPRICE ORDER BY DD_ID"
        df = query(sql, **kwargs)
        outdf = final_form(df, resm=False if resm is None else resm)
    elif dataset == "demand":
        sql = "SELECT HH_ID, DEMAND FROM EOSTABLE.DEMAND_TSD ORDER BY HH_ID"
        df = query(sql, **kwargs)
        df.rename({"DEMAND": "demand"}, axis=1, inplace=True)
        outdf = df.set_index("DateTime")
    elif dataset == "int":
        sql = "SELECT * FROM EOSTABLE.INTFLOW_HH ORDER BY HH_ID"
        df = query(sql, **kwargs)
        outdf = df.set_index("DateTime")
    elif dataset == "wind":
        sql = "SELECT HH_ID, SUM(LOAD) FROM EOSTABLE.PLANTLOAD{0} WHERE TEC_NAME = 'Wind (Offshore)' OR TEC_NAME = 'Wind (Onshore)' GROUP BY HH_ID ORDER BY HH_ID"
        load = query_years(sql, keep=True, **kwargs)
        sql = "SELECT MM_ID, SUM(CAPACITY) FROM EOSTABLE.PLANTCAPACITY WHERE TEC_NAME = 'Wind (Offshore)' OR TEC_NAME = 'Wind (Onshore)' GROUP BY MM_ID ORDER BY MM_ID"
        cap = query(sql, keep=True, **kwargs)
        load["MM_ID"] = load["HH_ID"].apply(lambda x: x // 1000000)
        capload = load.merge(cap, left_on="MM_ID", right_on="MM_ID")
        capload["wind_lf"] = capload["SUM(LOAD)"] / capload["SUM(CAPACITY)"]
        # capload = capload[['HH_ID', 'wind_lf']]
        capload = EOS_time(capload)
        outdf = capload
        # outdf = final_form(capload, resm=True if resm is None else resm)
    elif dataset == "solar":
        sql = "SELECT * FROM EOSTABLE.SOLAR ORDER BY HH_ID"
        df = query(sql, **kwargs)
        df["solar_lf"] = df["LOAD"] / df["CAPACITY"]
        # df = df[['DateTime', 'solar_lf']]
        df = final_form(df, resm=True if resm is None else resm)
        df = df.dropna()
        outdf = df
    elif dataset == "da" or dataset == "apx" or dataset == "sys":
        tables = {"apx": "EOSTABLE.APX_HH", "sys": "EOSTABLE.SYS_IMBALANCE_PRICE", "da": "EOSTABLE.NORDPOOL_N2EX_DA_HH"}
        renames = {"apx": {"APX": "apx"}, "sys": {"SYSPRICE": "sys"}, "da": {"VALUE": "da"}}
        sql = "SELECT * FROM " + tables[dataset] + " "
        if start_year or end_year:
            sql += "WHERE "
            if start_year:
                sql += "HH_ID >= " + str(start_year * 100000000) + " "
                if end_year:
                    sql += "AND "
            if end_year:
                sql += "HH_ID <= " + str((end_year + 1) * 100000000) + " "
        sql += "ORDER BY HH_ID"
        print(sql)
        # exit(0)
        df = query(sql, **kwargs)
        df.set_index("DateTime", inplace=True)
        df = df[~df.index.duplicated(keep="first")]
        outdf = df.rename(renames[dataset], axis=1)
    elif dataset == "dispatch":
        outdf = pd.concat(
            [
                prepare_data_db("da", start_year=start_year, end_year=end_year, resm=resm, **kwargs),
                prepare_data_db("apx", start_year=start_year, end_year=end_year, resm=resm, **kwargs),
                prepare_data_db("sys", start_year=start_year, end_year=end_year, resm=resm, **kwargs),
            ],
            axis=1,
            copy=False,
        )
    elif dataset == "ng_dem_all":
        sql = "SELECT * FROM EOSTABLE.NG_DEMAND_ALL ORDER BY HH_ID"
        df = query(sql, **kwargs)
        df = EOS_time(df)
        df.set_index("DateTime", inplace=True)
        outdf = df
    elif dataset == "capacity_mix":
        sql = "SELECT TRUNC(HH_ID/100000000) AS YEAR, SUM(LOAD)/2 AS PRODUCTION, TEC_NAME FROM EOSTABLE.PLANTLOAD{0} GROUP BY TRUNC(HH_ID/100000000), TEC_NAME ORDER BY TEC_NAME"
        tcg = query_years(sql)  # transmission connected generation
        sql = "SELECT TRUNC(HH_ID/100000000) AS YEAR, SUM(PUMP_STORAGE_PUMPING)/2 AS PUM, SUM(EMBEDDED_WIND_GENERATION)/2 AS EMWIN, SUM(EMBEDDED_SOLAR_GENERATION)/2 AS SOL FROM EOSTABLE.NG_DEMAND_ALL GROUP BY TRUNC(HH_ID/100000000) ORDER BY YEAR"
        emg = query(sql).set_index("YEAR")  # embedded generation
        tcg = tcg.pivot_table(values="PRODUCTION", columns="TEC_NAME", index="YEAR")
        print(tcg)
        gen = pd.concat([tcg, emg], axis=1, join="inner")
        print(emg)
        print(gen)
        gen["Wind (Onshore)"] += gen["EMWIN"]
        gen["Pumped Storage"] = gen["PUM"]
        gen["CCGT"] += gen["Gas CHP-CCGT"]
        gen.rename({"SOL": "Solar"}, axis=1, inplace=True)
        gen.drop(["PUM", "EMWIN", "Gas CHP-CCGT"], axis=1, inplace=True)
        gen.index.name = "Year"
        outdf = gen
    else:
        outdf = None
    return outdf


def log_run_data(file, model_type):
    try:
        version = subprocess.check_output(["git", "describe", "--tags"]).strip().decode("utf-8")
    except subprocess.CalledProcessError:
        version = "UNKNOWN"
    if not os.path.exists(file):
        with open(file, "w") as f:
            f.write("datetime,model_type,commit\n")
    with open(file, "a") as f:
        f.write(f"{datetime.now().isoformat()},{model_type},{version}\n")
