"""
This module uses the user provided inputs to aggregate FYR data in preparation for the Battery Dispatch GAMS code.
Authored: Luke Robins [luke.robins@auroraer.com]
"""
import sys
import os
import csv
from contextlib import closing
from subprocess import run
import shutil
import pandas as pd
import psycopg2
import numpy as np
from aer import get_dwh_password


def query(sql):
    """
    Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
    and closes the connection
    """
    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"
    dbname = "production"
    user = "eucurrency2014_production"
    password = response
    port = "5439"

    connect_string = (
        f"host={host} dbname={dbname} user={user} password={password} port={port}"
    )

    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sql, conn)
    return out_df


def hourly_EM_prices(scenario, reg, year_first, year_last):
    """
    Function to return the hourly Wholesale Electricity Prices from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, hhr.wholesaleprice
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.hourinyear <= 8760
                    ORDER BY t.date;"""

    print("Pulling wholesale electricity prices from DWH")
    WMprices = query(sql_query)

    if WMprices.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly prices in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    WMprices = WMprices.pivot(index="hourinyear", columns="year", values="wholesaleprice")

    return WMprices


def hourly_BM_prices(scenario, reg, year_first, year_last):
    """
    Function to return the hourly Balancing Prices from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, hhr.bmstochasticdispatchpricepermwh
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.hourinyear <= 8760
                    ORDER BY t.date;"""

    print("Pulling balancing prices from DWH")
    BMprices = query(sql_query)

    if BMprices.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly BM prices in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    BMprices = BMprices.pivot(index="hourinyear", columns="year", values="bmstochasticdispatchpricepermwh")

    return BMprices


def hourly_SNSP_fracs(scenario, reg, year_first, year_last):
    """
    Function to return the hourly system-non-synchronous fractions from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, hhr.nonsynchronouspenetrationfraction
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.hourinyear <= 8760
                    ORDER BY t.date;"""

    print("Pulling SNSP fractions from DWH")
    SNSPfracs = query(sql_query)

    if SNSPfracs.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly SNSP fractions in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    SNSPfracs = SNSPfracs.pivot(index="hourinyear", columns="year", values="nonsynchronouspenetrationfraction")

    return SNSPfracs


def hourly_Anc_prices(scenario, reg, year_first, year_last, DS3_final_regulated_year):
    """
    Function to return the hourly Ancillary DS3 Prices from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, anc.service, hha.clearingpricepermwperh
                    FROM HalfHourlyRegionAncillary hha
                    INNER JOIN time t on t.id = hha.timeid
                    INNER JOIN regions r on r.id = hha.regionid
                    INNER JOIN scenarios s on s.id = hha.scenarioid
                    INNER JOIN AncillaryServices anc on anc.id = hha.serviceid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.year <= '{DS3_final_regulated_year}'
                    and t.hourinyear <= 8760
                    and anc.service in ('SOR', 'FFR', 'POR', 'TOR1', 'TOR2')
                    ORDER BY t.date;"""

    print("Pulling DS3 Ancillary prices from DWH")
    Ancprices = query(sql_query)

    if Ancprices.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly DS3 prices in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    Ancprices = Ancprices.pivot_table(values="clearingpricepermwperh", index="hourinyear", columns=["year", "service"])

    return Ancprices


def yearly_Anc_tariffs(scenario, reg, year_first, year_last, DS3_final_regulated_year):
    """
    Function to return the yearly Ancillary Tariffs from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.year, anc.service, yra.ds3rescaledtariffinpricepermwh
                    FROM YearlyRegionAncillary yra
                    INNER JOIN time t on t.id = yra.timeid
                    INNER JOIN regions r on r.id = yra.regionid
                    INNER JOIN scenarios s on s.id = yra.scenarioid
                    INNER JOIN AncillaryServices anc on anc.id = yra.serviceid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.year <= '{DS3_final_regulated_year}'
                    and anc.service in ('SOR', 'FFR', 'POR', 'TOR1', 'TOR2')
                    ORDER BY t.year;"""

    print("Pulling DS3 Ancillary Tariffs from DWH")
    AncTariffs = query(sql_query)

    if AncTariffs.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly DS3 prices in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    AncTariffs = AncTariffs.pivot(index="year", columns="service", values="ds3rescaledtariffinpricepermwh")

    return AncTariffs


def hourly_NIVs(scenario, reg, year_first, year_last):
    """
    Function to return the hourly Net Imbalance Volumes from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, hhr.netimbalancevolumestochasticmw
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.hourinyear <= 8760
                    ORDER BY t.date;"""

    print("Pulling Stochastic Hourly NIVs from DWH")
    NIVs = query(sql_query)

    if NIVs.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly NIVs in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    NIVs = NIVs.pivot(index="hourinyear", columns="year", values="netimbalancevolumestochasticmw")

    return NIVs


def hourly_NIV_means(scenario, reg, year_first, year_last):
    """
    Function to return the hourly Net Imbalance Volumes from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, hhr.netimbalancevolumemeanmw
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' or t.year <= '{year_last}')
                    and t.hourinyear <= 8760
                    ORDER BY t.date;"""

    print("Pulling hourly NIV means from DWH")
    NIV_means = query(sql_query)

    if NIV_means.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly NIV means in the DWH."
            f" Maybe hourly data needs restoring or you have a typo in the run name?"
        )

    NIV_means = NIV_means.pivot(index="hourinyear", columns="year", values="netimbalancevolumemeanmw")

    return NIV_means


if __name__ == "__main__":

    path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    response = get_dwh_password()

    infile = 'InputAssembly.csv'
    report = pd.read_csv(infile, usecols=[0, 1, 2, 3, 4, 5, 6])

    for ind in report.index:
        ScenFolderName = report.loc[ind, 'shortname']
        RunName = report.loc[ind, 'RunName']
        RunFolder = report.loc[ind, 'location'][:-3]
        region = "IRX"
        year_first = str(report.loc[ind, 'year_first'])
        year_last = str(report.loc[ind, 'year_last'])
        DS3_final_regulated_year = str(report.loc[ind, 'DS3_Regulated_Final_Year'])
        Excel_outs = str(report.loc[ind, 'Excel_outs'])
        print(RunFolder)

        # Create a Scenario directory
        opath = path + "\Input_PriceSeries\\" + ScenFolderName
        if not os.path.exists(opath):
            os.makedirs(opath)
        else:
            print("\n" + "An Input_PriceSeries Scenario Folder named " + ScenFolderName + " already exists.")
            input("Press enter if you want to Overwrite it. Otherwise close this script and adjust InputAssembly.csv.")
            shutil.rmtree(opath)
            os.makedirs(opath)

        # Fetch EM hourly prices.
        EM_df = hourly_EM_prices(RunName, region, year_first, year_last)
        EM_df.to_csv(opath + "\\" + "reformat_EMPrices.csv")

        # Fetch BM hourly prices.
        BM_df = hourly_BM_prices(RunName, region, year_first, year_last)
        BM_df.to_csv(opath + "\\" + "reformat_BMPrices.csv")

        # Fetch hourly SNSP fracs.
        SNSP_df = hourly_SNSP_fracs(RunName, region, year_first, year_last)
        SNSP_df.to_csv(opath + "\\" + "reformat_SNSP_frac.csv")

        # Fetch Hourly AncPrices
        Anc_hh_df = hourly_Anc_prices(RunName, region, year_first, year_last, DS3_final_regulated_year)
        Anc_hh_df.to_csv(opath + "\\" + "reformat_Ancprices.csv", index_label=False)

        # Fetch Yearly AncTariffs
        AncTariff_yr_df = yearly_Anc_tariffs(RunName, region, year_first, year_last, DS3_final_regulated_year)
        AncTariff_yr_df.to_csv(opath + "\\" + "reformat_AncTariff.csv")

        # Fetch NIVs
        SNSP_df = hourly_NIVs(RunName, region, year_first, year_last)
        SNSP_df.to_csv(opath + "\\" + "reformat_NIVs.csv")

        # Fetch NIV means
        SNSP_df = hourly_NIV_means(RunName, region, year_first, year_last)
        SNSP_df.to_csv(opath + "\\" + "reformat_NIV means.csv")

        if int(Excel_outs) == 1:
            merge_file = "DataPrep\mergeIRXoutputs_IreRevjy.R"
            run(
                [
                    "Rscript",
                    merge_file,
                    "--ScenarioName",
                    ScenFolderName,
                    "--RunFolder",
                    RunFolder,
                    "--path",
                    path,
                    "--year_first",
                    year_first,
                    "--year_last",
                    year_last,
                ],
            ).check_returncode()
