# -*- coding: utf-8 -*-
"""
Created on 20_Jul_21

@author: Luke Robins.
"""

import pandas as pd
import psycopg2
from contextlib import closing
from pathlib import Path
import os
import numpy as np
import time
from aer import get_dwh_password


HISTORICAL_DATA_SCENARIOS = {
      "GBR": "Great British Power Market in GBP",
      "DEU": "German Power Market in EUR",
      "AUS": "Australian Power Market in AUD",
      "NSW": "Australian Power Market in AUD",
      "QLD": "Australian Power Market in AUD",
      "SAA": "Australian Power Market in AUD",
      "TAS": "Australian Power Market in AUD",
      "VIC": "Australian Power Market in AUD"
}

EARLIEST_HISTORICAL_DATA = {
      "GBR": 2015,
      "DEU": 2015,
      "AUS": 2015
}

TECH_LIST = ('coa', 'ccg')

TECH_STR = "("
a = 0
for t in TECH_LIST:
    TECH_STR = TECH_STR + "'" + t + "'"
    a = a + 1
    if a < len(TECH_LIST):
        TECH_STR = TECH_STR + ", "
TECH_STR = TECH_STR + ")"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Functions for reading from DWH
def query(sql):
    """ 
    Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
    and closes the connection
    """
    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"
    dbname = "production"
    user = "eucurrency2014_production"
    password = response
    port = "5439"

    connect_string = (
        f"host={host} dbname={dbname} user={user} password={password} port={port}"
    )

    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sql, conn)
    return out_df


def fetch_wholesale_DWH(scenario, reg, year_first, year_last, hist_or_mod):
    """
    Function to return the hourly Wholesale Electricity Prices from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    if hist_or_mod == "hist":
        price = "wholesaleprice"
    else:
        price = "shadowprice"

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, t.hourofday, t.halfhour, r.region, hhr.{price}
                    FROM HalfHourlyRegion hhr
                    INNER JOIN time t on t.id = hhr.timeid
                    INNER JOIN regions r on r.id = hhr.regionid
                    INNER JOIN scenarios s on s.id = hhr.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' and t.year <= '{year_last}')
                    ORDER BY t.date;"""

    print("Pulling electricity prices from DWH")
    elec_prices = query(sql_query)

    if elec_prices.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly prices in the DWH."
            f" Maybe half hourly data needs restoring or you have a typo in the run name?"
        )

    return elec_prices


def fetch_generation_DWH(scenario, reg, year_first, year_last):
    """
    Function to return the hourly plant generation from a given scenario and region.
    :param scenario:
    :param reg:
    :return:
    """

    # pull stuff from DWH
    sql_query = f"""SELECT t.date, t.year, t.hourinyear, t.hourofday, t.halfhour, r.region, tech.technology, sum(hhp.netProductionInmw) as prodinmw
                    FROM halfhourlyplant hhp
                    INNER JOIN time t on t.id = hhp.timeid
                    INNER JOIN plants p on p.id = hhp.plantid
                    INNER JOIN regions r on r.id = p.regionid
                    INNER JOIN technologies tech on tech.id = p.technologyid
                    INNER JOIN scenarios s on s.id = hhp.scenarioid
                    WHERE r.region = '{reg}' and s.scenario = '{scenario}'
                    and (t.year >= '{year_first}' and t.year <= '{year_last}')
                    and (tech.technology in {TECH_STR}) 
                    GROUP BY t.date, t.year, t.hourinyear, t.hourofday, t.halfhour, r.region, tech.technology
                    ORDER BY t.date, t.year, t.hourinyear, t.hourofday, t.halfhour, r.region, tech.technology;"""

    print("Pulling hourly plant generation data from DWH")
    hh_generation = query(sql_query)

    if hh_generation.empty:
        raise ValueError(
            f"Error: Scenario '{scenario}' does not have half hourly generation in the DWH."
            f" Maybe half hourly data needs restoring or you have a typo in the run name?"
        )

    return hh_generation


def KPI_daily_generation_profile(generation_df):
    """
    Function to find the annual daily generation profile for given hourly data.
    :return:
    """

    # Derive annual average generation:
    avg_df = generation_df.groupby(["year", "region", "technology"])["prodinmw"].mean().reset_index()

    # Derive annual average profile:
    generation_df = generation_df.groupby(["year", "hourofday", "halfhour", "region", "technology"])["prodinmw"].mean().reset_index()
    normalise_df = generation_df.copy()

    for (y, r, t) in zip(avg_df["year"], avg_df["region"], avg_df["technology"]):
        gen_locs = (generation_df['year'] == y) & (generation_df['region'] == r) & (generation_df['technology'] == t)
        avg_locs = (avg_df['year'] == y) & (avg_df['region'] == r) & (avg_df['technology'] == t)
        avg = avg_df.loc[avg_locs, "prodinmw"].values[0]
        normalise_df.loc[gen_locs, "prodinmw"] = generation_df.loc[gen_locs, "prodinmw"] / avg

    return normalise_df


def KPI_daily_price_curve(prices_df, hist_or_mod):
    """
    Function to find the annual daily price curve for given hourly data.
    :return:
    """

    if hist_or_mod == "hist":
        price = "wholesaleprice"
    else:
        price = "shadowprice"

    # Derive annual average price:
    avg_df = prices_df.groupby(["year", "region"])[price].mean().reset_index()

    # Derive annual average profile:
    prices_df = prices_df.groupby(["year", "hourofday", "halfhour", "region"])[price].mean().reset_index()
    normalise_df = prices_df.copy()

    for (y, r) in zip(avg_df["year"], avg_df["region"]):
        gen_locs = (prices_df['year'] == y) & (prices_df['region'] == r)
        avg_locs = (avg_df['year'] == y) & (avg_df['region'] == r)
        avg = avg_df.loc[avg_locs, price].values[0]
        normalise_df.loc[gen_locs, price] = prices_df.loc[gen_locs, price] / avg

    return normalise_df


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == "__main__":
    tic = time.time()

    response = get_dwh_password()

    infile = "scenarios_to_run.csv"
    report = pd.read_csv(infile, usecols=[0, 1, 2, 3, 4])

    for ind in report.index:
        ############
        # Start-up
        #
        # Read in region, scenarios
        # TODO Read in representative plants.

        output_folder = report.loc[ind, "output_folder"]
        scenario = report.loc[ind, "RunName"]
        region = report.loc[ind, "Region"]
        year_first = report.loc[ind, "year_first"]
        year_last = report.loc[ind, "year_last"]
        Path("outputs/" + output_folder).mkdir(parents=True, exist_ok=True)

        print(f"Starting the party for scenario '{scenario}' and region '{region}'")

        # Each region has a related historical data scenario
        # These are in the dict HISTORICAL_DATA_SCENARIOS
        # GBR, DEU, AUS, IBE.
        #   I have SQL queries that can access the historical data scenarios and grab the relevant data.
        # TODO  For IBE we'll need to take data from a saved folder instead of SQL.
        # TODO     Since IBE's data needs tidying up, I won't aim to have IBE data for Thursday.
        #
        # TODO Worth implementing some sort of "Automatic filter of outliers" to the historical data?
        #   Shave off all price periods with price >99th percentile?
        #
        # We'll be comparing historical back-cast runs to historical data.
        # TODO Only examine years where we have both.
        #   Figure out which years overlap:

        # Fetch the Data:
        # Keep in mind that AUS needs a regional index.
        print("Historical Data")
        # Fetch Half-Hourly Wholesale Prices
        hist_prices = fetch_wholesale_DWH(HISTORICAL_DATA_SCENARIOS[region], region, year_first, year_last, "hist")
        # Fetch Half-Hourly Generation (at a tech level)
        hist_generation = fetch_generation_DWH(HISTORICAL_DATA_SCENARIOS[region], region, year_first, year_last)

        print("Historical Backcasts")
        # Fetch Half-Hourly Wholesale Prices
        mod_prices = fetch_wholesale_DWH(scenario, region, year_first, year_last, "mod")
        # Fetch Half-Hourly Generation (at a tech level)
        mod_generation = fetch_generation_DWH(scenario, region, year_first, year_last)

        ############
        # Generate KPIs:
        #
        # Is monthly granularity worth it, or will it result in output bloat? Ideally, I think seasonal is best.
        #
        # Generation
        # We will apply the KPIs at a tech aggregated level, and also apply them to any specific "representative" plants
        # flagged by the respective product teams.

        hist_daily_gen_profiles = KPI_daily_generation_profile(hist_generation)
        mod_daily_gen_profiles = KPI_daily_generation_profile(mod_generation)

        # Total Plant Generation (Annual/Seasonal/Monthly)
        # Average Daily Generation Profile (Annual/Seasonal/Monthly) ###Priority
        #   CCGT and Coal.
        # Average production by price bucket supply curves (utilising @Johannes_Maywald's script)
        # Plotting change in generation against the actual generation (utilising @zelin.chen's script)
        #     ## Check that you know how to operate both of these.
        #
        # Prices
        # For modelled prices we would use shadow prices, as we want to remove the influence of uplift when examining
        # ramping behaviour.

        hist_daily_price_curves = KPI_daily_price_curve(hist_prices, "hist")
        mod_daily_price_curves = KPI_daily_price_curve(mod_prices, "mod")

        # Average daily spreads (Annual/Seasonal/Monthly)
        # Average daily price curve (Annual/Seasonal/Monthly) ###Priority
        # Annual PDCs

        ############
        # Output KPIs
        #
        # Excel outputs for flexibility.
        # Graphical Outputs for easy exports to PowerPoint.
        # Possible auto-slide-deck output? (I'd need to look up how to do this, probably best to do by hand for Thurs.)

        # Write-out
        outfile = "outputs/" + output_folder + "/daily.xlsx"
        with pd.ExcelWriter(outfile, engine='openpyxl', mode='a') as writer:
            hist_daily_gen_profiles.to_excel(writer, sheet_name="hist_gen")
            mod_daily_gen_profiles.to_excel(writer, sheet_name="mod_gen")
            hist_daily_price_curves.to_excel(writer, sheet_name="hist_pric")
            mod_daily_price_curves.to_excel(writer, sheet_name="mod_pric")

        ############
        # How should we present the outputs?
        # Graphical outputs can be presented as graphs.
        # Likewise anything that's a curve.
        # If you can pull out a correlation factor, do so.

